Rails.application.routes.draw do
  put '/tracker/location' => 'tracker_location#update'
  get '/assignments/paged' => 'assignments#paginated'
  get '/vehicles/paged' => 'vehicles#paginated'
  get '/drivers/paged' => 'drivers#paginated'
  resources :assignments
  resources :trackers
  resources :drivers
  resources :vehicles
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post '/register' => 'registration#create', as: 'registration'
  post '/login' => 'login#create', as: 'login'
  get '/current/user' => 'login#current_user'
  get '/stats' => 'stats#index'
end
