class TrackerLocationController < ApplicationController
  def update
    mobile_no = params[:mobile]
    tracker = Tracker.find_by mobile: mobile_no
    unless tracker.nil?
      location = TrackerLocation.new
      location.tracker = tracker
      location.lat = params[:lat]
      location.long = params[:long]
      location.save
    end
    return
  end

  private
  def tracker_params
    params.permit :mobile, :lat, :long
  end
end
