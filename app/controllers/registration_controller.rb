class RegistrationController < ApplicationController

  def create
    values = user_params
    user = User.new(values)
    user.active = true
    user.locked = false
    user_saved = user.save
    render status: :bad_request, json: {error: user.errors.messages} unless user_saved
  end

  private

  def user_params
    params.require(:registration).permit :username, :password, :password_confirmation
  end

  def profile_params
    params.require(:registration).permit :email

  end

end
