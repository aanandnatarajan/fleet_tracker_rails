class LoginController < ApplicationController

  include ActionController::Cookies

  # Creates a login session
  def create
    u = User.find_by(username: login_params[:username]).try(:authenticate, login_params[:password])
    unless u
      render status: :unauthorized, json: {error: "Bad Credentials"}
    else
      cookies.encrypted['X-AUTH-TOKEN'] = {
        :value => u.username,
        :expires => 2.hours.from_now.utc,
        :httponly => true
      }
      puts cookies
      render status: :ok, json: {username: u.username}
    end
  end

  def current_user
    token = cookies.encrypted['X-AUTH-TOKEN']
    puts "Token #{token}"
    if token
      user = User.find_by(username: token)
      user.password_digest = nil
      render json: user
    else
      render status: :unauthorized, json: {message: "unauthorized"}
    end
  end

  private
  def login_params
    params.require(:login).permit :username, :password
  end

end
