class StatsController < ApplicationController

  def index
    render json: {
      vehicles: Vehicle.count,
      drivers: Driver.count,
      trackers: Tracker.count,
      assignments: Assignment.count
    }
  end
end
