class Profile < ApplicationRecord
	belongs_to :user
	has_and_belongs_to_many :work_locations
end
