class Tracker < ApplicationRecord
  has_many :tracker_installation
  has_many :tracker_locations
end
