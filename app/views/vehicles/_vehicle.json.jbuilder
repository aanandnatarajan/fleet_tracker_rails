json.extract! vehicle, :id, :registration_no, :make, :model, :category, :created_at, :updated_at
json.url vehicle_url(vehicle, format: :json)
