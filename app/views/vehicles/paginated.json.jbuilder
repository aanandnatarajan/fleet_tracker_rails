json.columns [
    {
      label: 'Vehicle #',
      field: 'id',
    },
    {
      label: 'Registration',
      field: 'registration_no',
    },
    {
      label: 'Make',
      field: 'make',
    },
    {
      label: 'Model',
      field: 'model',
    },
    {
      label: 'Category',
      field: 'category',
    },
  ]
json.rows @paginated, partial: 'vehicles/paged_vehicle', as: :vehicle