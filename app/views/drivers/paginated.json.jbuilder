json.columns [
    {
      label: 'Driver #',
      field: 'id',
    },
    {
      label: 'Firstname',
      field: 'first_name',
    },
    {
      label: 'Lastname',
      field: 'last_name',
    },
    {
      label: 'License No',
      field: 'license_no',
    },
    {
      label: 'Mobile',
      field: 'mobile',
    },
  ]
json.rows @paginated, partial: 'drivers/paged_driver', as: :driver