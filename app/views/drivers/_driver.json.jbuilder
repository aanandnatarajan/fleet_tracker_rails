json.extract! driver, :id, :first_name, :middle_name, :last_name, :license_no, :license_expiry_date, :date_of_birth, :created_at, :updated_at
json.url driver_url(driver, format: :json)
