json.columns [
        {
          label: 'Trip #',
          field: 'id',
        },
        {
          label: 'Customer Name',
          field: 'customer_name',
        },
        {
          label: 'Customer Mobile',
          field: 'customer_mobile',
        },
        {
          label: 'Driver Name',
          field: 'driver_name',
        },
        {
          label: 'Vehicle Number',
          field: 'vehicle_no',
        },
        {
          label: 'Pickup Time',
          field: 'pickup_time',
        },
        {
          label: 'Pickup Location',
          field: 'pickup_location',
        },
        {
          label: 'Drop Location',
          field: 'drop_location',
        },
        {
          label: 'Completed?',
          field: 'is_complete',
        }
      ]
json.rows @paginated, partial: 'assignments/paged_assignment', as: :assignment