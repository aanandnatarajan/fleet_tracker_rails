json.extract! assignment, :id, :customer_name, :customer_mobile, :pickup_location, :drop_location, :pickup_time, :created_at, :updated_at
json.driver do
  json.first_name assignment.driver.first_name
  json.mobile assignment.driver.mobile
end
json.vehicle do
  json.vehicle_no assignment.vehicle.registration_no
  json.model assignment.vehicle.model
  json.category assignment.vehicle.category
end
json.url assignment_url(assignment, format: :json)
