json.extract! tracker, :id, :mobile, :sim_no, :created_at, :updated_at
json.url tracker_url(tracker, format: :json)
