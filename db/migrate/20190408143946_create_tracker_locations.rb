class CreateTrackerLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :tracker_locations do |t|
      t.belongs_to :tracker
      t.decimal :lat, {:precision=>10, :scale=>6}
      t.decimal :long, {:precision=>10, :scale=>6}
      t.timestamps
    end

    add_foreign_key :tracker_locations, :trackers
  end
end
