class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :first_name, index: true
      t.string :last_name, index: true
      t.string :email, index: true
      t.string :phone, index: true
      t.date :date_of_birth

      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
