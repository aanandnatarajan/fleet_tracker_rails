class AddCustomerToAssignment < ActiveRecord::Migration[5.2]
  def change
    change_table :assignments do |t|
      t.string :customer_name
      t.string :customer_mobile
    end
  end
end
