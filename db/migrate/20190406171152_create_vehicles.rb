class CreateVehicles < ActiveRecord::Migration[5.2]
  def change
    create_table :vehicles do |t|
      t.string :registration_no
      t.string :make
      t.string :model
      t.string :category
      t.datetime :start_date

      t.timestamps
    end
  end
end
