class CreateDrivers < ActiveRecord::Migration[5.2]
  def change
    create_table :drivers do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :license_no
      t.date :license_expiry_date
      t.string :mobile
      t.date :date_of_birth
      t.timestamps
    end
  end
end
