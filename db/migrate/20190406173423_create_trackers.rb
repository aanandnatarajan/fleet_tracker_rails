class CreateTrackers < ActiveRecord::Migration[5.2]
  def change
    create_table :trackers do |t|
      t.string :mobile
      t.string :sim_no
      
      t.timestamps
    end

  end
end
