class CreateTrackerInstallations < ActiveRecord::Migration[5.2]
  def change
    create_table :tracker_installations do |t|
      t.belongs_to :tracker
      t.belongs_to :vehicle
      t.timestamps
    end

    add_foreign_key :tracker_installations, :trackers
    add_foreign_key :tracker_installations, :vehicles
  end
end
