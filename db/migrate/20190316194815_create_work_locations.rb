class CreateWorkLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :work_locations do |t|
      t.string :name

      t.timestamps
    end

    create_table :profiles_work_locations, id: false do |t|
    	t.belongs_to :profile, index: true
    	t.belongs_to :work_location, index: true
    end
  end
end
