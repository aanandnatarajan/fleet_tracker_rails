class CreateAssignments < ActiveRecord::Migration[5.2]
  def change
    create_table :assignments do |t|
      t.string :pickup_location
      t.string :drop_location
      t.belongs_to :driver
      t.belongs_to :vehicle
      t.datetime :pickup_time
      t.boolean :is_complete
      t.timestamps
    end
    add_foreign_key :assignments, :drivers
    add_foreign_key :assignments, :vehicles

  end
end
